<?php
/**
 * Created by IntelliJ IDEA.
 * User: Denisek
 * Date: 5/16/2018
 * Time: 6:02 AM
 */

require_once 'assets/config/config.php';
require_once 'functions.php';

$role=2;
include_once "menu.php";

session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
//unset($_SESSION['count']);
if( isset($_SESSION['email'])){

}else{
    header("location:index.php");
}

$curl = curl_init();
$json = json_decode(callAPI("GET",$api."/users"), true);
?>

    <!DOCTYPE html>
    <html lang="sk">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Admin page</title>
        <link rel="icon" type="image/jpg" href="assets/img/favicon.png">
        <link rel="stylesheet" href="assets/css/bootstrap/bootstrap_v4-0-0-beta-2.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
        <link rel="stylesheet" href="assets/css/style.css">
    </head>
    <body>
        <div class="container">
            <div class="container">
                <h2 class="text-center">Pridať novú trasu</h2>
                <div class="row">
                    <div class="col-12">
                        <div id="map" style="height:350px;" ></div>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-6">
                            <form target="adminindex.php" method="post">
                                <div class="row mt-2">
                                    <div class="col-4">
                                        <input class="form-control input-sm" name="Name" type="text" placeholder="Názov trasy">
                                    </div>
                                    <div class="col-4">
                                        <input id="origin-input" class="form-control controls  controls input-sm" type="text" placeholder="Začiatok trasy">
                                    </div>

                                    <div class="col-4">
                                        <input id="destination-input" class="form-control controls input-sm" type="text" placeholder="Koniec trasy">
                                    </div>
                                    <input type="hidden" name="Distance" id="hiDistance">
                                    <input type="hidden" name="Mode" id="mode" value="2">
                                    <input type="hidden" name="StartLocLat" id="hiStartLocLat">
                                    <input type="hidden" name="StartLocLng" id="hiStartLocLng">
                                    <input type="hidden" name="EndLocLat" id="hiEndLocLat">
                                    <input type="hidden" name="EndLocLng" id="hiEndLocLng">
                                    <input type="hidden" name="StartState" id="hiStartState">
                                    <input type="hidden" name="EndState" id="hiEndState">
                                </div>
                                <div class="btn-group btn-group-toggle mt-2" data-toggle="buttons">
                                    <label class="btn btn-secondary ml-2">
                                        <input type="radio" name="options" value="public" class="public" autocomplete="off" onchange="toggleTeam()"> Verejná
                                    </label>
                                    <label class="btn btn-secondary active ">
                                        <input type="radio" name="options" id="relay" value="relay" class="group" autocomplete="off" checked onchange="toggleTeam()"> Štafeta
                                    </label>
                                </div>
                                <div id="relay-race">
                                    <a href="adminindex.php?inc=TRUE"
                                        <?php if (isset($_GET['inc']) && intval($_SESSION['count']) < 6){
                                            $_SESSION['count']++;
                                            if($_SESSION['count'] == 6)
                                                echo 'style="visibility:hidden;"';
                                        } else echo 'style="visibility:visible"';?> class="btn btn-secondary">Pridať</a>
                                    <a href="adminindex.php?dec=TRUE"
                                        <?php if (isset($_GET['dec']) && intval($_SESSION['count']) > 2){
                                            $_SESSION['count']--;
                                            if($_SESSION['count'] == 2)
                                                echo 'style="visibility:hidden;"';
                                        } else echo 'style="visibility:visible"';?> class="btn btn-secondary">Ubrať</a>
                                    <?php
                                    if(!isset($_SESSION['count'])){
                                        $_SESSION['count'] = 2;
                                    }
                                    else{
                                        for ($i = 0; $i < $_SESSION['count']; $i++){
                                            echo '<div class="form-group">
                                        <select class="form-control mt-2" id="exampleFormControlSelect1"  name="'.$i.'" required>
                                          <option selected disabled>Vyberte bežca</option>';
                                            foreach ($json as $node){
                                                echo "<option value={$node["id"]}>" . $node["name"]." " . $node["surname"]. "</option>";
                                            }
                                            echo '</select>
                                                </div>';
                                        }
                                    }
                                    ?>
                                </div>
                                <button type="submit" class="btn btn-secondary" name="add">Vytvoriť trasu</button>
                            </form>
                        </div>
                        <?php

                        if( isset($_POST['add'])){
                            $data['name'] = $_POST['Name'];
                            $data['distance'] = $_POST['Distance'];
                            $data['mode'] = $_POST['Mode'];
                            $data['startLocLat'] = $_POST['StartLocLat'];
                            $data['startLocLng'] = $_POST['StartLocLng'];
                            $data['endLocLat'] = $_POST['EndLocLat'];
                            $data['endLocLng'] = $_POST['EndLocLng'];
                            $data['startState'] = $_POST['StartState'];
                            $data['endState'] = $_POST['EndState'];
                            if($_POST['options'] == "relay"){
                                for ($i = 0; $i<$_SESSION['count']; $i++){
                                    $runner['id'] = $_POST[$i];
                                    $data['runners'][$i] = $runner;
                                }
                               $result = json_decode(callAPI("POST", $api . "/".$_SESSION['id']."/add-route/",http_build_query($data)));

                            } else{
                                $result = json_decode(callAPI("POST", $api . "/".$_SESSION['id']."/add-route/",http_build_query($data)));
                            }

                        }
                        ?>
                        <div class="col-6">
                            <H2 class="mt-2">Pridať do noviniek</H2>
                            <form method="post">
                                <input type="text" class="form-control mt-2" id="new" name="newheader" placeholder="Nadpis">
                                <textarea class="form-control mt-2" id="new" placeholder="Text správy" name="new" rows="5"></textarea>
                                <button type="submit" class="btn btn-secondary mt-2">Pridať správu</button>
                                <div id="newSuccess"></div>
                            </form>
                            <form target="adminindex.php" action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post" enctype="multipart/form-data">
                                <div class="form-group form-inline mt-2">
                                    <input type="file" class="form-control-file" name="file" id="file">
                                    <button type="submit" class="btn btn-secondary" value="upload" name="submit">Pridať používateľov</button>
                                </div>
                            </form>

                            <?php
                            if ( isset($_POST["submit"]) ) {

                                if ( isset($_FILES["file"]) && $_FILES["file"]["type"] == "application/vnd.ms-excel") {

                                    //if there was an error uploading the file
                                    if ($_FILES["file"]["error"] > 0) {
                                        echo "Return Code: " . $_FILES["file"]["error"] . "<br />";

                                    }
                                    else {
                                        //if file already exists
                                        if (file_exists("upload/" . $_FILES["file"]["name"])) {
                                            echo $_FILES["file"]["name"] . " already exists. ";
                                        }
                                        else {
                                            $request = array();
                                            $tmpName = $_FILES['file']['tmp_name'];
                                            $csv = file_get_contents($tmpName);
                                            $csvAsArray = explode("\n", $csv);
                                            array_shift($csvAsArray);
                                            for ($i = 0; $i<sizeof($csvAsArray); $i++) {
                                                $csvAsArray[$i] = explode(";", $csvAsArray[$i]);
                                            }

                                            foreach ($csvAsArray as $node){

                                                    $data['surname'] = $node[1];
                                                    $data['name'] = $node[2];
                                                    $data['email'] = $node[3];
                                                    $data['schoolName'] = $node[4];
                                                    $data['schoolAddress'] = $node[5];
                                                    $data['street'] = $node[6];
                                                    $data['psc'] = $node[7];
                                                    $data['city'] = $node[8];

                                                    array_push($request, $data);
                                            }
                                            $req['data'] = $request;
                                            $result = json_decode(callAPI("POST", $api ."/import-users", http_build_query($req)));
                                            echo "<script type='text/javascript'>alert('Používatelia úspešne pridaní')</script>";
                                        }
                                    }
                                } else {
                                    echo "<script type='text/javascript'>alert('Neplatný typ súboru/ nebol zvolený súbor');</script>";
                                }
                            }

                            if (isset($_POST['new']) && isset($_POST['newheader']))
                            {
                                $data['new']=$_POST['new'];
                                $data['newheader']=$_POST['newheader'];
                                $result = json_decode(callAPI("POST", $api.'/news',$data));
                                $text = "<p style='color: green;' class='text-center'>Správa bola pridaná do feedu a odoslaná používateľom</p>";
                                echo '<script type="text/javascript">
                window.onload = function()
                {
                    document.getElementById("newSuccess").innerHTML = "' . $text . '";
                }
        </script>';
                            }
                            ?>

                    </div>
                </div>

            <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group row">
                                <div class="col-6"><label for="inputDistance">Dĺžka trasy v km:</label></div>
                                <div class="col-6"><input type="text" class="form-control" id="inputDistance" disabled></label></div>
                            </div>
                            <div class="form-group row">
                                <div class="col-6"><label for="inputStartCoords">Súradnice štartu:</label></div>
                                <div class="col-6"><input type="text" class="form-control" id="inputStartCoords" disabled></label></div>
                            </div>
                            <div class="form-group row">
                                <div class="col-6"><label for="inputEndCoords">Súradnice cieľa:</label></div>
                                <div class="col-6"><input type="text" class="form-control" id="inputEndCoords" disabled></label></div>
                            </div>
                            <div class="form-group row">
                                <div class="col-6"><label for="inputStartState">Štartovná krajina:</label></div>
                                <div class="col-6"><input type="text" class="form-control" id="inputStartState" disabled></label></div>
                            </div>
                            <div class="form-group row">
                                <div class="col-6"><label for="inputEndState">Cieľová krajina:</label></div>
                                <div class="col-6"><input type="text" class="form-control" id="inputEndState" disabled></label></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <script>
                function toggleTeam() {

                    var box = document.getElementById("relay");
                    var relay = document.getElementById("relay-race");
                    var mode = document.getElementById("mode");

                    if (box.checked === true){
                        relay.style.display = "block";
                        mode.value = "2";

                    } else {
                        relay.style.display = "none";
                        mode.value = "0";
                    }
                }

                function initMap() {
                    var stredSK = {lat: 48.75, lng: 19.45};
                    var map = new google.maps.Map(document.getElementById('map'), {
                        mapTypeControl: false,
                        center: stredSK,
                        zoom: 7
                    });

                    new AutocompleteDirectionsHandler(map);
                }

                /**
                 * @constructor
                 */
                function AutocompleteDirectionsHandler(map) {
                    this.map = map;
                    this.originPlaceId = null;
                    this.destinationPlaceId = null;
                    var originInput = document.getElementById('origin-input');
                    var destinationInput = document.getElementById('destination-input');
                    this.directionsService = new google.maps.DirectionsService;
                    this.directionsDisplay = new google.maps.DirectionsRenderer;
                    this.directionsDisplay.setMap(map);

                    var originAutocomplete = new google.maps.places.Autocomplete(
                        originInput, {placeIdOnly: true});
                    var destinationAutocomplete = new google.maps.places.Autocomplete(
                        destinationInput, {placeIdOnly: true});

                    this.setupPlaceChangedListener(originAutocomplete, 'ORIG');
                    this.setupPlaceChangedListener(destinationAutocomplete, 'DEST');

                }

                AutocompleteDirectionsHandler.prototype.setupPlaceChangedListener = function(autocomplete, mode) {
                    var me = this;
                    autocomplete.bindTo('bounds', this.map);
                    autocomplete.addListener('place_changed', function() {
                        var place = autocomplete.getPlace();
                        if (!place.place_id) {
                            window.alert("Prosím vyberte možnosť z dropdown listu.");
                            return;
                        }
                        if (mode === 'ORIG') {
                            me.originPlaceId = place.place_id;
                        } else {
                            me.destinationPlaceId = place.place_id;
                        }
                        me.route();
                    });

                };

                AutocompleteDirectionsHandler.prototype.route = function() {
                    if (!this.originPlaceId || !this.destinationPlaceId) {
                        return;
                    }
                    var me = this;

                    this.directionsService.route({
                        origin: {'placeId': this.originPlaceId},
                        destination: {'placeId': this.destinationPlaceId},
                        travelMode: 'WALKING'
                    }, function(response, status) {
                        if (status === 'OK') {
                            me.directionsDisplay.setDirections(response);

                            var distance = response.routes[0].legs[0].distance.value;
                            distance = distance.valueOf() / 1000;
                            console.log(distance);
                            var start_location_lat = response.routes[0].legs[0].start_location.lat().toString();
                            var start_location_lng = response.routes[0].legs[0].start_location.lng();
                            var end_location_lat = response.routes[0].legs[0].end_location.lat();
                            var end_location_lng = response.routes[0].legs[0].end_location.lng();
                            var start_state = response.routes[0].legs[0].start_address.split(',')[response.routes[0].legs[0].start_address.split(',').length-1];
                            var end_state = response.routes[0].legs[0].end_address.split(',')[response.routes[0].legs[0].end_address.split(',').length-1];

                            document.getElementById("inputDistance").value = distance;
                            document.getElementById("inputStartCoords").value = start_location_lat.toString() + ", " + start_location_lng.toString();
                            document.getElementById("inputEndCoords").value = end_location_lat.toString() + ", " + end_location_lng.toString();
                            document.getElementById("inputStartState").value = start_state;
                            document.getElementById("inputEndState").value = end_state;

                            document.getElementById("hiDistance").value = distance;
                            document.getElementById("hiStartLocLat").value = start_location_lat.toString();
                            document.getElementById("hiStartLocLng").value = start_location_lng.toString();
                            document.getElementById("hiEndLocLat").value = end_location_lat.toString();
                            document.getElementById("hiEndLocLng").value = end_location_lng.toString();
                            document.getElementById("hiStartState").value = start_state;
                            document.getElementById("hiEndState").value = end_state;

                            $('#modal').modal('show');


                        } else {
                            window.alert('Nepodarilo sa vytvoriť trasu kvôli ' + status);
                        }
                    });
                };
            </script>

            <script async defer
                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAsMZqqT3Azn1HQF5tk8pG6BYW9Qp4s6LY&libraries=places&callback=initMap">
            </script>
            <script src="assets/js/bootstrap/jquery-3-2-1-slim.js"></script>
            <script src="assets/js/bootstrap/popper-1-12-3.js"></script>
            <script src="assets/js/bootstrap/bootstrap_v4-0-0-beta-2.js"></script>
        </div>
    </body>
</html>
