<?php
/**
 * Created by IntelliJ IDEA.
 * User: Denisek
 * Date: 5/16/2018
 * Time: 4:49 PM
 */

$role=2;
include_once "menu.php";

require_once 'assets/config/config.php';
require_once 'functions.php';
?>
<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="UTF-8">
    <title>Používatelia</title>
    <link rel="icon" type="image/jpg" href="assets/img/favicon.png">
    <link rel="stylesheet" href="assets/css/bootstrap/bootstrap_v4-0-0-beta-2.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
    <div class="container">
        <div class="container-fluid">
                <h2 class="text-center">Používatelia</h2>
            <?php

            $curl = curl_init();
            $json = json_decode(callAPI("GET",$api."/users"), true);

            echo "<div class=\"table-responsive\">
                            <table class=\"table table-hover table-striped\">
                                <thead>
                                <tr>
                                    <th>Meno</th>
                                    <th>Priezvisko</th>
                                    <th>Email</th>
                                    <th>Škola</th>
                                    <th>Adresa Školy</th>
                                    <th>Mesto</th>
                                    <th>Ulica</th>
                                </tr>
                                </thead>
                                <tbody>";
            foreach ($json as $node){
                echo "<tr onclick=window.location.assign('userstats.php?uid={$node["id"]}')>
                                 <td>" . $node["name"]. "</td>
                                 <td>" . $node["surname"]. "</td>
                                 <td>" . $node["email"]. "</td>
                                 <td>" . $node["schoolName"]. "</td>
                                 <td>" . $node["schoolAddress"]. "</td>
                                 <td>" . $node["city"]. "</td>
                                 <td>" . $node["street"]. "</td>
                                 </tr>";
            }

            ?>
        </div>
    </div>



    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAsMZqqT3Azn1HQF5tk8pG6BYW9Qp4s6LY&libraries=places&callback=initMap">
    </script>
    <script src="assets/js/bootstrap/jquery-3-2-1-slim.js"></script>
    <script src="assets/js/bootstrap/popper-1-12-3.js"></script>
    <script src="assets/js/bootstrap/bootstrap_v4-0-0-beta-2.js"></script>
</body>
</html>