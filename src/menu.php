<?php
/**
 * Created by IntelliJ IDEA.
 * User: Samko
 * Date: 19.5.2018
 * Time: 17:12
 */
if(isset($_SESSION['id']))
{
    $uid = $_SESSION['id'];
}
echo "<nav class=\"navbar navbar-expand-lg navbar-light bg-light\">
    <div class=\"navbar-brand\">RunTracker</div>
    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\" aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        <span class=\"navbar-toggler-icon\"></span>
    </button>
    <div class=\"collapse navbar-collapse\" id=\"navbarNav\">
        <ul class=\"navbar-nav\">";

if($role == 0)
{
    echo "
            <li class=\"nav-item active\">
                <a class=\"nav-link\" href=\"index.php\">Domov</a>
            </li>
            <li class=\"nav-item\">
                <a class=\"nav-link\" href=\"login.php\">Prihlásenie</a>
            </li>
            <li class=\"nav-item\">
                <a class=\"nav-link\" href=\"register.php\">Registrácia</a>
            </li>
            <li class=\"nav-item\">
                <a class=\"nav-link\" href=\"about.php\">O aplikácii</a>
            </li>";

    if($map==1) {
        echo "<li class=\"nav-item dropdown\">
                <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                Mapa
                </a>
                <div id=\"mapSelect\" class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">
                    <a class=\"dropdown-item\" href=\"#\">Bydliská</a>
                    <a class=\"dropdown-item\" href=\"#\">Školy</a>
            </li>";
    }
}
if($role == 1)
{
    echo "<li class=\"nav-item\">
                <a class=\"nav-link\" href=\"userindex.php\">Domov</a>
            </li>
            <li class=\"nav-item\">
                <a class=\"nav-link\" href=\"useraddroute.php\">Vytvorenie behu</a>
            </li>
            <li class=\"nav-item\">
                <a class=\"nav-link\" href=\"userstats.php?uid=".$uid."\">Štatistiky</a>
            </li>
            <li class=\"nav-item\">
                <a class=\"nav-link\" href=\"logout.php\">Odhlásenie</a>
            </li>";
}
if($role ==2)
{
    echo "<li class=\"nav-item\">
                <a class=\"nav-link\" href=\"adminindex.php\">Domov</a>
            </li>
            <li class=\"nav-item\">
                <a class=\"nav-link\" href=\"userlist.php\">Zoznam používateľov</a>
            </li>
            <li class=\"nav-item\">
                <a class=\"nav-link\" href=\"logout.php\">Odhlásenie</a>
            </li>";

}






echo "</ul>
</div>
</nav>";
