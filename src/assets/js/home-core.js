//default set po nacitani
var selText = "Bydliská";
var data;
var map;
var geocoder;
var markers = [];

//linky k restom
var urlBydliska = 'http://webtech.sromovsky.sk/final/public/index.php/addresses';
var urlSkoly = 'http://webtech.sromovsky.sk/final/public/index.php/schools';

//defaultny request
var xhttp = new XMLHttpRequest();
xhttp.open("GET", urlBydliska, false);
xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
xhttp.send();
data = JSON.parse(xhttp.responseText);

var loader = document.getElementById('loader');
loader.style.height = window.outerHeight + 'px';
loader.style.display="block";

function startWorker()
{
    document.getElementById('progress').style.display="block";
    console.log(document.getElementById('progress'));
    if (typeof(Worker) !== "undefined")
    {

        w = new Worker("assets/js/worker.js");
        var message = data.length;
        w.postMessage(message);
        for (var i = 0; i < markers.length; i++)
        {
            markers[i].setMap(null);
        }
        w.onmessage = function (e) {
            if (e.data === "finito") {
                document.getElementById('progress').style.display = "none";
                console.log(document.getElementById('progress'));
                w.terminate();
            }
            else {
                google.maps.event.trigger(map, 'resize');
                if (selText === "Bydliská") {
                    console.log(data[e.data].city + " " + data[e.data].street + " " + data[e.data].psc);
                    geocoder.geocode({'address': data[e.data].city + " " + data[e.data].street + " " + data[e.data].psc}, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            var marker = new google.maps.Marker({
                                map: map,
                                position: results[0].geometry.location
                            });
                            markers.push(marker);
                            google.maps.event.trigger(map, 'resize');
                        } else {
                            //  alert('Geocode was not successful for the following reason: ' + status);
                        }
                    });
                }
                else {
                    console.log(data[e.data].name + " " + data[e.data].address);
                    geocoder.geocode({'address': data[e.data].name + " " + data[e.data].address}, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            var marker = new google.maps.Marker({
                                map: map,
                                position: results[0].geometry.location
                            });
                            markers.push(marker);
                            google.maps.event.trigger(map, 'resize');
                        }
                        else {
                            // alert('Geocode was not successful for the following reason: ' + status);
                        }
                    });
                }
            }
        }
    }
}


//pri zmene formulari, dynamicky zmen data
function changeData()
{
    if (selText === "Bydliská") {
        xhttp.open("GET", urlBydliska, false);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send();
        data = JSON.parse(xhttp.responseText);
    }
    else {
        xhttp.open("GET", urlSkoly, false);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send();
        data = JSON.parse(xhttp.responseText);
    }
}

function initMap()
{
    geocoder = new google.maps.Geocoder();
    var stredSK = {lat: 48.75, lng: 19.45};
    map = new google.maps.Map(document.getElementById('map'),
        {
            zoom: 7.7,
            center: stredSK
        });
    startWorker();
    //len def. funkcie, nechcel som davat ako anonymous
    $("#mapSelect a").click(function (e)
    {
        console.log("change");
        e.preventDefault(); // cancel the link behaviour
        if (selText === $(this).text()) {
            return;
        }
        else {
            selText = $(this).text();
            changeData();
            startWorker();
        }
    });
}
