<?php
/**
 * Created by IntelliJ IDEA.
 * User: Samko
 * Date: 15.5.2018
 * Time: 18:31
 */
?>
<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Dokumentácia</title>
    <link rel="icon" type="image/jpg" href="assets/img/favicon.png">
    <link rel="stylesheet" href="assets/css/bootstrap/bootstrap_v4-0-0-beta-2.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>

<?php
$role=0;
include_once 'menu.php';
?>

<div class="container mb-5" id="wrapper">
    <h2 class="text-center py-3">
        Dokumentácia aplikácie
    </h2>
    <div class="row">
        <section id="sidebar" class="col-3 mb-0">
            <blockquote class="blockquote">
                <p class="mb-0">Táto aplikácia oživila moju stratenú lásku k behaniu.</p>
                <footer class="blockquote-footer">Donald Trump<cite title="Source Title"> 17.5.2018 v rozhovore pre Russia Today</cite></footer>
            </blockquote>
        </section>
    <section id="main" class="col-9">
    <p>Táto aplikácia bola vytvorená v rámci skúškového projektu na predmet Webové Technológie 2. Na tejto stránke sú uvedené základné
        informácie o verzionovacom systéme a aj základná technická dokumentácia. Autormi aplikácie sú: </p>
        <ul>
            <li>Filip Ujčík</li>
            <li>Stanislav Bekeš</li>
            <li>Denis Šmátrala</li>
            <li>Tomáš Šromovský</li>
            <li>Samuel Šušoliak</li>
        </ul>
    </section>
    </div>
    <div class="row">
        <h5>Verzionovací systém</h5>
        <p>Podľa zadania tohto projektu je nutné v rámci dokumentácie uviesť link na náš verzionovací systém, ktorý
        sme počas vypracovávania používali. Využili sme Bitbucket, ktorý nám slúžil veľmi dobre.
            Repozitár je dostupný <a href="https://bitbucket.org/stufeiteam15/webtech-final/src/master/">TU</a>.
        Pre prístup k nahliadnutiu je však nutné sa do Bitbucketu prihlásiť.
        </p>
    </div>
    <div class="row">
        <h5>Technická dokumentácia aplikácie</h5>
        <p>
            Prácu na aplikácii bolo možné rozdeliť na vytváranie používateľského rozhrania a na prácu na back-ende.
            Front-end aplikácie je implementovaný pomocou HTML (verzie 5), Javascript (ES2015), jQuery (verzie 3-2-1) a Boostrapu (verzie 4). Jednotlivé stránky samozrejme
            komunikujú so serverom pomocou PHP.
        </p>
        <p>
            Back-end aplikácie je implementovaný pomocou Frameworku Laravell Lumen (5.5.2), ktorý zabezpečuje zázemie pre vytváranie RESTových
            služieb nad využitou MySQL databázou. Príklad jedného z RESTových volaní je <a href="http://webtech.sromovsky.sk/final/public/index.php/addresses">TU</a>.

            Zoznam naimplementovaných REST služieb aj s ich účelom:
        <ul>
            <li><em>/has-valid-login</em> - Validácia loginu.</li>
            <li><em>/has-valid-reg</em> - Zaregistrovanie resp. validáciu registrácie</li>
            <li><em>/{id}/setpassword</em> - Nastavenie hesla používateľovi</li>
            <li><em>/addresses</em> - Získanie zoznamu adries používateľov</li>
            <li><em>/schools</em> - Získanie zoznamu škôl používateľov</li>
            <li><em>/{id}/routes[-{active}]</em> - Na získanie všetkých aktívnych ciest používateľa</li>
            <li><em>/{id}/add-route</em> - Na pridanie novej cesty</li>
            <li><em>/import-users</em> - Pre importovanie userov zo súboru</li>
        </ul>
        </p>
    </div>
    <div class="row">
        <h5>Používateľské rozhranie aplikácie</h5>
        <p>
            UI používateľa zobrazuje všetky dostupné informácie o trasách urečných pre nahliadnutie používateľovi. Na pravej
            strane domovskej stránky sú tiež zobrazené novinky, tak ako to od nás požadovalo zadanie. Používateľ tiež môže
            trasy vytvárať, zobrazovať aktuálny progres svojho behu na nich a tiež aj tento progres generovať po pridaní
            odbehnutých kilometrov k jemu vybranej trase. Používateľovi sú tiež dostupné aj jeho osobné štatistiky.
        </p>
    </div>
    <div class="row">
        <h5>Administrátorské rozhranie aplikácie</h5>
        <p>
            Administrátor aplikácie, môže registrovať nových používateľov buď priamo pomocou databázy alebo pomocou rozhrania
            pre uploadnutie csv. súboru v predpísanom tvare. Môže vytvárať nové štafetové a verejné trasy a tiež aj novinky,
            ktoré sú používateľom zobrazované v ich rozhraní. Novinky pridané administrátorom sú používateľom taktiež
            doručované aj na ich email. Administrátor má tiež prístup k globálnym štatistikám v aplikácii, t.j.
            v jeho rozhraní môže zobrazovať štatistiky každého jedného registrovaného v aplikácii.
        </p>
    </div>
</div>
    <script src="assets/js/bootstrap/jquery-3-2-1-slim.js"></script>
<script src="assets/js/bootstrap/popper-1-12-3.js"></script>
<script src="assets/js/bootstrap/bootstrap_v4-0-0-beta-2.js"></script>

</body>