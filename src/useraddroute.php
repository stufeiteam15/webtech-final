<?php
/**
 * Created by IntelliJ IDEA.
 * User: Denisek
 * Date: 5/16/2018
 * Time: 6:02 AM
 */

require_once 'assets/config/config.php';
require_once 'functions.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();
if( isset($_SESSION['email']))
{

}else{
    header("location:index.php");
}
if(empty($_SESSION)) {
    session_start();
}
//logout if set logout on button
if(isset($_GET['logout']))
{
    unset($_SESSION['email']);;
}

if(isset($_SESSION['email']))
{

}else{
    header("location:index.php");
}
?>

<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Stránka spravcu</title>
    <link rel="icon" type="image/jpg" href="assets/img/favicon.png">
    <link rel="stylesheet" href="assets/css/bootstrap/bootstrap_v4-0-0-beta-2.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>

<?php $role=1;
include_once 'menu.php';
?>

<div class="container" id="wrapper">
    <div class="row">
        <div class="col-12">
            <H2>Pridať novú trasu</H2>
            <div id="map" style="height:350px;" ></div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <form method="post">
                <div class="form-group">
                    <label for="inputName">Názov trasy</label>
                    <input type="text" class="form-control" id="inputName" name="Name" placeholder="Beh devin" required>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="origin-input">Začiatok trasy</label>
                        <input id="origin-input" class="form-control controls  controls input-sm" type="text" placeholder="Bratislava" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="destination-input">Koniec trasy</label>
                        <input id="destination-input" class="form-control controls input-sm" type="text" placeholder="Trnava" required>
                    </div>
                </div>
                <input type="hidden" name="Distance" id="hiDistance">
                <input type="hidden" name="Mode" value="1">
                <input type="hidden" name="StartLocLat" id="hiStartLocLat">
                <input type="hidden" name="StartLocLng" id="hiStartLocLng">
                <input type="hidden" name="EndLocLat" id="hiEndLocLat">
                <input type="hidden" name="EndLocLng" id="hiEndLocLng">
                <input type="hidden" name="StartState" id="hiStartState">
                <input type="hidden" name="EndState" id="hiEndState">
                <button type="submit" class="btn btn-secondary">Vytvoriť trasu</button>
            </form>
        </div>
    </div>
    <?php

    if(isset($_POST['Name'])){
        $data['name'] = $_POST['Name'];
        $dist_string = $_POST['Distance'];
        //$dist_string=floatval($dist_string);
        $data['distance'] =$dist_string;
        $data['mode'] = $_POST['Mode'];
        $data['startLocLat'] = $_POST['StartLocLat'];
        $data['startLocLng'] = $_POST['StartLocLng'];
        $data['endLocLat'] = $_POST['EndLocLat'];
        $data['endLocLng'] = $_POST['EndLocLng'];
        $data['startState'] = $_POST['StartState'];
        $data['endState'] = $_POST['EndState'];


       // var_dump($data);
        echo '<br>';
       // var_dump($api . "".$_SESSION['id']."/add-route/");
        echo '<br>';
        $result = json_decode(callAPI("POST", $api . "".$_SESSION['id']."/add-route/",$data));
        echo '<br>';
      //  var_dump($result);

    }
    ?>


    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Informácie o vašej trase</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-6"><label for="inputDistance">Dĺžka trasy v km:</label></div>
                        <div class="col-6"><input type="text" class="form-control" id="inputDistance" disabled></label></div>
                    </div>
                    <div class="form-group row">
                        <div class="col-6"><label for="inputStartCoords">Súradnice štartu:</label></div>
                        <div class="col-6"><input type="text" class="form-control" id="inputStartCoords" disabled></label></div>
                    </div>
                    <div class="form-group row">
                        <div class="col-6"><label for="inputEndCoords">Súradnice cieľa:</label></div>
                        <div class="col-6"><input type="text" class="form-control" id="inputEndCoords" disabled></label></div>
                    </div>
                    <div class="form-group row">
                        <div class="col-6"><label for="inputStartState">Štartovná krajina:</label></div>
                        <div class="col-6"><input type="text" class="form-control" id="inputStartState" disabled></label></div>
                    </div>
                    <div class="form-group row">
                        <div class="col-6"><label for="inputEndState">Cieľová krajina:</label></div>
                        <div class="col-6"><input type="text" class="form-control" id="inputEndState" disabled></label></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function toggleTeam() {

            var box = document.getElementById("relay");

            var relay = document.getElementById("relay-race");

            if (box.checked === true){
                relay.style.display = "block";
            } else {
                relay.style.display = "none";
            }
        }

        function initMap() {
            var stredSK = {lat: 48.75, lng: 19.45};
            var map = new google.maps.Map(document.getElementById('map'), {
                mapTypeControl: false,
                center: stredSK,
                zoom: 7
            });

            new AutocompleteDirectionsHandler(map);
        }

        /**
         * @constructor
         */
        function AutocompleteDirectionsHandler(map) {
            this.map = map;
            this.originPlaceId = null;
            this.destinationPlaceId = null;
            var originInput = document.getElementById('origin-input');
            var destinationInput = document.getElementById('destination-input');
            this.directionsService = new google.maps.DirectionsService;
            this.directionsDisplay = new google.maps.DirectionsRenderer;
            this.directionsDisplay.setMap(map);

            var originAutocomplete = new google.maps.places.Autocomplete(
                originInput, {placeIdOnly: true});
            var destinationAutocomplete = new google.maps.places.Autocomplete(
                destinationInput, {placeIdOnly: true});

            this.setupPlaceChangedListener(originAutocomplete, 'ORIG');
            this.setupPlaceChangedListener(destinationAutocomplete, 'DEST');

        }

        AutocompleteDirectionsHandler.prototype.setupPlaceChangedListener = function(autocomplete, mode) {
            var me = this;
            autocomplete.bindTo('bounds', this.map);
            autocomplete.addListener('place_changed', function() {
                var place = autocomplete.getPlace();
                if (!place.place_id) {
                    window.alert("Prosím vyberte možnosť z dropdown listu.");
                    return;
                }
                if (mode === 'ORIG') {
                    me.originPlaceId = place.place_id;
                } else {
                    me.destinationPlaceId = place.place_id;
                }
                me.route();
            });

        };

        AutocompleteDirectionsHandler.prototype.route = function() {
            if (!this.originPlaceId || !this.destinationPlaceId) {
                return;
            }
            var me = this;

            this.directionsService.route({
                origin: {'placeId': this.originPlaceId},
                destination: {'placeId': this.destinationPlaceId},
                travelMode: 'WALKING'
            }, function(response, status) {
                if (status === 'OK') {
                    me.directionsDisplay.setDirections(response);

                    var distance = response.routes[0].legs[0].distance.value;
                    distance = distance.valueOf() / 1000;
                    console.log(distance);
                    var start_location_lat = response.routes[0].legs[0].start_location.lat().toString();
                    var start_location_lng = response.routes[0].legs[0].start_location.lng();
                    var end_location_lat = response.routes[0].legs[0].end_location.lat();
                    var end_location_lng = response.routes[0].legs[0].end_location.lng();
                    var start_state = response.routes[0].legs[0].start_address.split(',')[response.routes[0].legs[0].start_address.split(',').length-1];
                    var end_state = response.routes[0].legs[0].end_address.split(',')[response.routes[0].legs[0].end_address.split(',').length-1];

                    document.getElementById("inputDistance").value = distance;
                    document.getElementById("inputStartCoords").value = start_location_lat.toString() + ", " + start_location_lng.toString();
                    document.getElementById("inputEndCoords").value = end_location_lat.toString() + ", " + end_location_lng.toString();
                    document.getElementById("inputStartState").value = start_state;
                    document.getElementById("inputEndState").value = end_state;

                    document.getElementById("hiDistance").value = distance;
                    document.getElementById("hiStartLocLat").value = start_location_lat.toString();
                    document.getElementById("hiStartLocLng").value = start_location_lng.toString();
                    document.getElementById("hiEndLocLat").value = end_location_lat.toString();
                    document.getElementById("hiEndLocLng").value = end_location_lng.toString();
                    document.getElementById("hiStartState").value = start_state;
                    document.getElementById("hiEndState").value = end_state;

                    $('#modal').modal('show');


                } else {
                    window.alert('Nepodarilo sa vytvoriť trasu kvôli ' + status);
                }
            });
        };
    </script>

    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAsMZqqT3Azn1HQF5tk8pG6BYW9Qp4s6LY&libraries=places&callback=initMap">
    </script>
    <script src="assets/js/bootstrap/jquery-3-2-1-slim.js"></script>
    <script src="assets/js/bootstrap/popper-1-12-3.js"></script>
    <script src="assets/js/bootstrap/bootstrap_v4-0-0-beta-2.js"></script>

</body>
</html>

