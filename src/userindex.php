<?php
/**
 * Created by IntelliJ IDEA.
 * User: filip
 * Date: 5/11/2018
 * Time: 8:21 PM
 */
if(empty($_SESSION)) {
    session_start();
}

if(isset($_SESSION['email']))
{

}
else{
    header("location:index.php");
}

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once 'assets/config/config.php';
require_once 'functions.php';

$newsletters = json_decode(callAPI("GET",$api."/news"));
$cieleActive = json_decode(callAPI("GET",$api."/".$_SESSION['id']."/routes-active"));
$cieleInactive = json_decode(callAPI("GET",$api."/".$_SESSION['id']."/routes-inactive"));

function nameOfType($type){
    switch ($type) {
        case 0:
            return "verejný";
        case 1:
            return "privátny";
        case 2:
            return "štafetový";
        default:
            return "nedefinovaný";
    }
}

if (isset($_POST['idRoute'])&&isset($_POST['mode'])){                                                                   //Sluzi na tlacidla aktivovat trasu
    if($_POST['type']=='activate'){
        $data['idroute'] = $_POST['idRoute'];
        $result = json_decode(callAPI("POST", $api . "/".$_SESSION['id']."/activate-route",$data));
    }
    else{
        $data['id_trasa'] = $_POST['idRoute'];
        $result = json_decode(callAPI("POST", $api . "".$_SESSION['id']."/deactivate-route",$data));
    }

    $cieleActive = json_decode(callAPI("GET",$api."/".$_SESSION['id']."/routes-active"));
    $cieleInactive = json_decode(callAPI("GET",$api."/".$_SESSION['id']."/routes-inactive"));
}

?>
<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Stránka používateľa</title>
    <link rel="icon" type="image/jpg" href="assets/img/favicon.png">
    <link rel="stylesheet" href="assets/css/bootstrap/bootstrap_v4-0-0-beta-2.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>

<?php

$role=1;
include_once 'menu.php';
?>

<div class="container" id="wrapper">
    <div class="row">
        <div class="col-8">

            <h3>Vitajte v aplikácii, ste úspešne prihlásený.</h3>
            <h4>Vaše aktívne behy:</h4>

            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Názov behu</th>
                    <th scope="col">Mód</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                <?php
                reloadActive($cieleActive);
                function reloadActive($cieleActive){
                    foreach ($cieleActive as $ciel){
                        echo '
                      <tr>
                        <td><a href="route.php?route='.$ciel->id.'&mode='.$ciel->mode.'&active=1">'.$ciel->name.'</a></td>
                        <td>'.nameOfType($ciel->mode).'</td> 
                        <td>
                            <form method="post">
                                <input type="hidden" name="idRoute" value="' . $ciel->id . '">
                                <input type="hidden" name="mode" value="' . $ciel->mode . '">
                                <input type="hidden" name="type" value="deactivate">
                                <input class="form-control controls input-sm" type="submit" name="submit" value="Deaktivovať">
                            </form>
                        </td>
                      </tr>';
                    }
                }

                ?>
                </tbody>
            </table>

            <h4>Vaše neaktívne behy:</h4>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Názov behu</th>
                    <th scope="col">Mód</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                <?php
                reloadInactive($cieleInactive);
                function reloadInactive($cieleInactive)
                {
                    foreach ($cieleInactive as $ciel) {
                        echo '
                      <tr>
                        <td><a href="route.php?route='.$ciel->id.'&mode='.$ciel->mode.'&active=0">' . $ciel->name . '</a></td>
                        <td>' . nameOfType($ciel->mode) . '</td> 
                        <td>
                            <form method="post">
                                <input type="hidden" name="idRoute" value="' . $ciel->id . '">
                                <input type="hidden" name="mode" value="' . $ciel->mode . '">
                                <input type="hidden" name="type" value="activate">
                                <input class="form-control controls input-sm" type="submit" name="submit" value="Aktivovať">
                            </form>
                        </td>
                      </tr>';
                    }
                }
                ?>
                </tbody>
            </table>


        </div>

        <div class="col-4">
            <?php
            foreach ($newsletters as $newsletter){
                echo '<div class="card" style="width: 18rem;">
                            <div class="card-body">
                                <h5 class="card-title">'.$newsletter->date.' '.$newsletter->header.'</h5>
                                <p class="card-text">'.$newsletter->new.'</p>
                             </div>
                          </div>';
            }

            //logout logika


            ?>
        </div>
    </div>
</div>


<script src="assets/js/bootstrap/jquery-3-2-1-slim.js"></script>
<script src="assets/js/bootstrap/popper-1-12-3.js"></script>
<script src="assets/js/bootstrap/bootstrap_v4-0-0-beta-2.js"></script>

</body>
</html>
