<?php
/**
 * Created by IntelliJ IDEA.
 * User: filip
 * Date: 5/17/2018
 * Time: 8:12 PM
 */
require_once 'assets/config/config.php';
require_once 'functions.php';

session_start();
if( isset($_SESSION['email'])&& isset($_GET['rid'])){

}else{
    header("location:index.php");
}
?>
    <!DOCTYPE html>
    <html lang="sk">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>RunTracker</title>
        <link rel="icon" type="image/jpg" href="assets/img/favicon.png">
        <link rel="stylesheet" href="assets/css/bootstrap/bootstrap_v4-0-0-beta-2.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
        <link rel="stylesheet" href="assets/css/style.css">
    </head>
    <body>

    <div class="container">
        <form method="post">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputDistance">Počet odbehnutých kilometrov</label>
                    <input type="number" class="form-control" id="inputDistance" name="distance" required>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputDate">Deň tréningu</label>
                    <input type="date" class="form-control" id="inputDate" name="date">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="inputStartTime">Čas začiatku tréningu</label>
                    <input type="time" class="form-control" id="inputStartTime" name="startTime">
                </div>
                <div class="form-group col-md-4">
                    <label for="inputEndTime">Čas konca tréningu</label>
                    <input type="time" class="form-control" id="inputEndTime" name="endTime">
                </div>
                <div class="form-group col-md-4">
                    <label for="inputRating">Subjektívne hodnotenie tréningu od 1 po 5</label>
                    <input type="number" min="1" max="5" class="form-control" id="inputRating" name="rating">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-3">
                    <label for="inputStartLat">Štart zemepísna šírka</label>
                    <input type="text" class="form-control" id="inputStartLat" name="startLat">
                </div>
                <div class="form-group col-md-3">
                    <label for="inputStartLng">Štart zemepísna dĺžka</label>
                    <input type="text" class="form-control" id="inputStartLng" name="startLng">
                </div>
                <div class="form-group col-md-3">
                    <label for="inputEndLat">Koniec zemepísna šírka</label>
                    <input type="text" class="form-control" id="inputEndLat" name="endLat">
                </div>
                <div class="form-group col-md-3">
                    <label for="inputEndLng">Koniec zemepísna dĺžka</label>
                    <input type="text" class="form-control" id="inputEndLng" name="endLng">
                </div>
            </div>

            <div class="form-group">
                <label for="inputNote">Poznámka</label>
                <input type="text" class="form-control" id="inputNote" name="note">
            </div>

            <button type="submit" class="btn btn-primary">Odoslať tréning</button>
        </form>

        <a href="route.php?route=<?php echo $_GET['rid']?>&mode=<?php echo $_GET['mode']?>&active=1"><button class="btn btn-primary">Späť na progress</button></a>
    </div>

    <script src="assets/js/home-core.js"></script>
    <script src="assets/js/bootstrap/jquery-3-2-1-slim.js"></script>
    <script src="assets/js/bootstrap/popper-1-12-3.js"></script>
    <script src="assets/js/bootstrap/bootstrap_v4-0-0-beta-2.js"></script>
    </body>
    </html>

<?php
if( isset($_POST['distance'])){
    $data['distance'] = $_POST['distance'];
    $data['startTime'] = $_POST['date'] ." ". $_POST['startTime'];
    $data['endTime'] = $_POST['date'] ." ". $_POST['endTime'];
    $data['rating'] = $_POST['rating'];
    $data['startLon'] = $_POST['startLat'];
    $data['startLng'] = $_POST['startLng'];
    $data['endLon'] = $_POST['endLat'];
    $data['endLng'] = $_POST['endLng'];
    $data['note'] = $_POST['note'];
    $result = json_decode(callAPI("POST", $api . "".$_SESSION['id']."/".$_GET['rid']."/add-training/",$data));
}



?>