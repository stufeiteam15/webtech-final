<?php
/**
 * Created by IntelliJ IDEA.
 * User: Samko
 * Date: 10.5.2018
 * Time: 14:39
 */
//login page
?>
<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Prihlásenie</title>
    <link rel="icon" type="image/jpg" href="assets/img/favicon.png">
    <link rel="stylesheet" href="assets/css/bootstrap/bootstrap_v4-0-0-beta-2.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
<?php
$role=0;
include_once 'menu.php';
?>
<div class="container" id="wrapper">
    <h2 class="text-center py-5">
        Prihlásenie do aplikácie
    </h2>
    <form method="post">
        <div class="row justify-content-center">
            <div class="col-auto">
                <div class="form-group row">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email">
                </div>
                <div class="form-group row">
                    <label for="password">Heslo</label>
                    <input type="password" class="form-control" id="password" name="password">
                </div>
                <div id = "errorDiv">
                </div>
                <div class="row justify-content-center py-2">
                    <button type="submit" class="btn btn-success btn-block">Prihlásenie</button>
                </div>
                <div class="row justify-content-center py-2">
                    <a href="register.php" class="btn btn-info btn-block">Registrácia</a>
                </div>
    </form>
</div>
<?php
require_once 'assets/config/config.php';
require_once 'functions.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if(isset($_POST['email']))
{

    $data['email']=$_POST['email'];
    $data['password']=$_POST['password'];

    //$result = json_decode(callAPI("POST", $apiTest . "has-valid-login.php",$data));

    //test restcall
    $result = json_decode(callAPI("POST", 'http://webtech.sromovsky.sk/final/public/index.php/has-valid-login',$data));

    if ($result->value)
    {
        //inicializacia session
        session_start();
        $_SESSION['email']= $_POST['email'];
        $_SESSION['id']=$result->userId;

        //podla role prepinanie stranok na redirect
        switch ($result->role)
        {
            case 0:
                header("location:setpassword.php");
                break;
            case 1:
                header("location:userindex.php");
                break;
            case 2:
                $_SESSION['admin']=1;
                header("location:adminindex.php");
                break;
        }
    } else
        {
        //ak v jsone false tak vypisanie takejto chyby
        $text = "<p style='color: red;'></i> Nesprávny email alebo heslo</p>";
        echo '<script type="text/javascript">
        window.onload = function()
        {
            document.getElementById("errorDiv").innerHTML = "' . $text . '";
        }
</script>';
    }
}

?>




<script src="assets/js/bootstrap/jquery-3-2-1-slim.js"></script>
<script src="assets/js/bootstrap/popper-1-12-3.js"></script>
<script src="assets/js/bootstrap/bootstrap_v4-0-0-beta-2.js"></script>

</body>
