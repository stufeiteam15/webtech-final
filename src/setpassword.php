<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Prihlásenie</title>
    <link rel="icon" type="image/jpg" href="assets/img/favicon.png">
    <link rel="stylesheet" href="assets/css/bootstrap/bootstrap_v4-0-0-beta-2.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <style>
        .vertical-center {
            height: 100%;
            width: 100%;
            text-align: center;
            background-color: transparent;

            padding-bottom: 20px;
            padding-top: 40px;
        }
        #message {
            height: 100%;
            width: 100%;
            text-align: center;
            display:block;
            background: #f1f1f1;
            color: #000;
            position: relative;
            padding: 20px;
            margin-top: 10px;
        }

        #message p {
            padding: 0px 45px;
            font-size: 12px;
        }


        .valid {
            color: green;
        }

        .valid:before {
            position: relative;
            content: "✔";
        }


        .invalid {
            color: red;

        }

        .invalid:before {
            position: relative;
            content: "✖";
        }
    </style>
</head>
<body>

<div class="container">

    <div class="jumbotron vertical-center" id="tittle">
        <h2 class="display-5">Pred prvým prihlásenim si prosím zmeňte heslo</h2>
    </div>
    <form method="post">
        <div class="form-row justify-content-center">
            <div class="col-md-4 mb-3">
                <label for="psw">Zadajte nové heslo</label>
                <input type="password" class="form-control" id="psw" name="psw" placeholder="New password"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required>
            </div>
        </div>
        <div class="form-row justify-content-center">
            <div class="col-md-4 mb-3">
                <label for="confirm">Potvrdte heslo</label>
                <input type="password" class="form-control" id="confirm" name="confirm" placeholder="Confirm"  required>
            </div>
        </div>
        <div class="form-row justify-content-center">
            <button class="btn btn-primary" type="submit" id="butt" name="butt">Zmeniť</button> 
        </div>
    </form>

<div id="message" class="justify-content-center">
    <h5>Heslo musí spĺňať nasledujúce požiadavký:</h5>
    <p id="letter" class="invalid">Musí obsahovať <b>malé</b> písmeno</p>
    <p id="capital" class="invalid">Musí obsahovať <b>velké</b> písmeno</p>
    <p id="number" class="invalid">Musí obsahovať <b>číslo</b></p>
    <p id="length" class="invalid">Minimálne <b>8 znakov</b></p>
    <p id="match" class="invalid">Heslá sa musia <b>zhodovať</b></p>
</div>




</div>
<?php
require_once 'assets/config/config.php';
require_once 'functions.php';

if(empty($_SESSION)) // if the session not yet started
    session_start();

if(!isset($_SESSION['email'])) {
    header("Location:login.php");
    exit;
}

if ($_POST['psw']) {
    if($_POST['psw'] == $_POST['confirm']) {
        $data['password'] = $_POST['psw'];

        $result = json_decode(callAPI("POST", $api . $_SESSION['id'] . "/setpassword", $data));
        header("Location: userindex.php");
        exit;
    }
}
?>
<script>
    var myInput = document.getElementById("psw");
    var myInput2 = document.getElementById("confirm");
    var letter = document.getElementById("letter");
    var capital = document.getElementById("capital");
    var number = document.getElementById("number");
    var length = document.getElementById("length");
    var mtch = document.getElementById("match");

    var a=0;
    var b=0;
    var c=0;
    var d=0;
    var e=0;


    // When the user starts to type something inside the password field
    myInput.onkeyup = function() {
        // Validate lowercase letters
        var lowerCaseLetters = /[a-z]/g;
        if(myInput.value.match(lowerCaseLetters)) {
            letter.classList.remove("invalid");
            letter.classList.add("valid");
            a=1;
        } else {
            letter.classList.remove("valid");
            letter.classList.add("invalid");
            a=0;
        }

        // Validate capital letters
        var upperCaseLetters = /[A-Z]/g;
        if(myInput.value.match(upperCaseLetters)) {
            capital.classList.remove("invalid");
            capital.classList.add("valid");
            b=1;
        } else {
            capital.classList.remove("valid");
            capital.classList.add("invalid");
            b=0;
        }

        // Validate numbers
        var numbers = /[0-9]/g;
        if(myInput.value.match(numbers)) {
            number.classList.remove("invalid");
            number.classList.add("valid");
            c=1;
        } else {
            number.classList.remove("valid");
            number.classList.add("invalid");
            c=0;
        }

        // Validate length
        if(myInput.value.length >= 8) {
            length.classList.remove("invalid");
            length.classList.add("valid");
            d=1;
        } else {
            length.classList.remove("valid");
            length.classList.add("invalid");
            d=0;
        }
        if(document.getElementById("psw").value == document.getElementById("confirm").value){
            mtch.classList.remove("invalid");
            mtch.classList.add("valid");
            e=1;
        }
        else {
            mtch.classList.remove("valid");
            mtch.classList.add("invalid");
            e=0;

        }
        if(a==1 && b==1 && c==1 && d==1 && e==1){
            document.getElementById("butt").disabled = false;
        }
        else
            document.getElementById("butt").disabled = true;
    }
    myInput2.onkeyup = function() {
        if(document.getElementById("psw").value == document.getElementById("confirm").value){
            mtch.classList.remove("invalid");
            mtch.classList.add("valid");
            e=1;
        }
        else {
            mtch.classList.remove("valid");
            mtch.classList.add("invalid");
            e=0;

        }
        if(a==1 && b==1 && c==1 && d==1 && e==1){
            document.getElementById("butt").disabled = false;
        }
        else
            document.getElementById("butt").disabled = true;
    }

</script>

<script src="assets/js/bootstrap/jquery-3-2-1-slim.js"></script>
<script src="assets/js/bootstrap/popper-1-12-3.js"></script>
<script src="assets/js/bootstrap/bootstrap_v4-0-0-beta-2.js"></script>

</body>
</html>