<?php
/**
 * Created by IntelliJ IDEA.
 * User: Samko
 * Date: 10.5.2018
 * Time: 21:25
 */
//register page
?>

<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Registrácia</title>
    <link rel="icon" type="image/jpg" href="assets/img/favicon.png">
    <link rel="stylesheet" href="assets/css/bootstrap/bootstrap_v4-0-0-beta-2.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
<?php
$role=0;
$map = 0;
include_once 'menu.php';

?>
<div class="container" id="wrapper">
    <h2 class="text-center py-5">
        Registrácia
    </h2>
    <form method="post">
        <h4>Základné údaje</h4>
            <div class="form-row">
                <div class="form-group col-lg-4">
                    <label for="email">Email <i style="color: red; size: 2px" class="fas fa-asterisk"></i></label>
                    <div class="input-group">
                    <span class="input-group-addon" id="zavinac">@</span>
                    <input type="email" class="form-control" id="email" name="email" aria-describedby="zavinac" required>
                    </div>
                </div>
                <div class="form-group col-lg-4">
                    <label for="name">Meno <i style="color: red; size: 2px" class="fas fa-asterisk"></i></label>
                    <input type="text" class="form-control" id="name" name="name" required>
                </div>
                <div class="form-group col-lg-4">
                    <label for="surname">Priezvisko <i style="color: red; size: 2px" class="fas fa-asterisk"></i></label>
                    <input type="text" class="form-control" id="surname" name="surname" required>
                </div>
            </div>
        <h4>Údaje o škole</h4>
            <div class="form-row">
                <div class="form-group col-lg-6">
                    <label for="school">Meno strednej školy</label>
                    <input type="text" class="form-control" id="school" name="school">
                </div>
                <div class="form-group col-lg-6">
                    <label for="schoolAddress">Adresa školy</label>
                    <input type="text" class="form-control" id="schoolAddress" name="schoolAddress">
                </div>
            </div>
        <h4>Bydlisko</h4>
            <div class="form-row">
                <div class="form-group col-lg-5">
                    <label for="street">Ulica <i style="color: red; size: 2px" class="fas fa-asterisk"></i></label>
                    <input type="text" class="form-control" id="street" name="street" required>
                </div>
                <div class="form-group col-lg-5">
                    <label for="city">Obec <i style="color: red; size: 2px" class="fas fa-asterisk"></i></label>
                    <input type="text" class="form-control" id="city" name="city" required>
                </div>
                <div class="form-group col-lg-2">
                    <label for="psc">PSČ <i style="color: red; size: 2px" class="fas fa-asterisk"></i></label>
                    <input type="text" class="form-control" id="psc" name="psc" required>
                </div>
            </div>
        <div class="form-row justify-content-center py-4">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="false" id="newsletter" name="newsletter" >
                <label class="form-check-label" for="newsletter">
                   Mám záujem o Newsletter aplikácie
                </label>
            </div>
        </div>
        <div class="form-row justify-content-center">
            <button type="submit" class="btn btn-primary btn">Registruj</button>
        </div>
        <div id="errorDiv">

        </div>
    </form>
    <div class="modal" id="regModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Registrácia prebehla úspešne</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Na email Vám bolo odoslané Vaše heslo, ktoré je nutné pri prvom prihlásaní zmeniť. Potvrďte pre presmerovanie na prihlásenie</p>
                    <p><i style="color: red;" class="fas fa-exclamation-triangle"></i> POZOR ! Skontrolujte aj priečinok SPAM</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="location.href='login.php'">OK</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="assets/js/bootstrap/jquery-3-2-1-slim.js"></script>
<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once 'assets/config/config.php';
require_once 'functions.php';

if (isset($_POST['email']))
{
    $data['email']=$_POST['email'];
    $data['name']=$_POST['name'];
    $data['surname']=$_POST['surname'];
    $data['schoolName']=$_POST['school'];
    $data['schoolAddress']=$_POST['schoolAddress'];
    $data['street']=$_POST['street'];
    $data['city']=$_POST['city'];
    $data['psc']=$_POST['psc'];

    if(isset($_POST['newsletter']))
    {
        $data['newsletter']=true;
    }
    else
    {
        $data['newsletter']=false;
    }


    //print_r($data);

    $result = json_decode(callAPI("POST", $api."has-valid-reg",$data));

    if($result->value)
    {
        echo '<script language="javascript">';
        echo '$(document).ready(function () {
        $(\'#regModal\').modal(\'show\');
    });';
        echo '</script>';
    }
    else
    {
        $text = "<p style='color: red;' class='text-center'>Chyba ! Tento email už v aplikácii niekto používa.</p>";
        echo '<script type="text/javascript">
        window.onload = function()
        {
            document.getElementById("errorDiv").innerHTML = "' . $text . '";
        }
</script>';
    }
}
?>
<script src="assets/js/bootstrap/popper-1-12-3.js"></script>
<script src="assets/js/bootstrap/bootstrap_v4-0-0-beta-2.js"></script>

</body>


