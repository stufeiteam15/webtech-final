<?php
/**
 * Created by IntelliJ IDEA.
 * User: filip
 * Date: 5/18/2018
 * Time: 12:13 PM
 */
require_once 'assets/config/config.php';
require_once 'functions.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();
if( isset($_SESSION['email']) && isset($_GET['mode']) && isset($_GET['route'])){

}else{
    header("location:index.php");
}
$idRoute = $_GET['route'];
$route = json_decode(callAPI("GET",$api."routes/".$idRoute));


if($_GET['mode'] == 0 || $_GET['mode'] == 1){
    $mode = 0;

    $trainings = json_decode(callAPI("GET",$api.$_SESSION['id']."/".$idRoute."/progessone"));
}
else {
    $mode = 2;
    $trainings = json_decode(callAPI("GET", $api . $idRoute ."/progressteam"));
}

function color($user){
    switch ($user){
        case 0:
            return 'bg-primary';
        case 1:
            return 'bg-secondary';
        case 2:
            return 'bg-success';
        case 3:
            return 'bg-danger';
        case 4:
            return 'bg-warning';
        case 5:
            return 'bg-info';
        default:
            return '';
    }
}

function colorLegend($user){
    switch ($user){
        case 0:
            return 'text-primary';
        case 1:
            return 'text-secondary';
        case 2:
            return 'text-success';
        case 3:
            return 'text-danger';
        case 4:
            return 'text-warning';
        case 5:
            return 'text-info';
        default:
            return '';
    }
}

$name = $route->name;
$finalDistance = $route->distance;
$actualDistance = 0;

?>

<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Stránka používateľa</title>
    <link rel="icon" type="image/jpg" href="assets/img/favicon.png">
    <link rel="stylesheet" href="assets/css/bootstrap/bootstrap_v4-0-0-beta-2.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="navbar-brand">RunTracker</div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="userindex.php">Domov</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="useraddroute.php">Vytvorenie behu</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="userstats.php?uid=<?php echo $_SESSION['id'];?>">Štatistiky</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="logout.php">Odhlásenie</a>
            </li>
        </ul>
    </div>
</nav>

<div class="container" id="wrapper">
    <h1>Zobrazujem progress pre <?php echo $name; ?>:</h1>
    <div class="progress" style="height: 30px;">
        <?php
        if($mode==0){
            foreach ($trainings as $training){
                if ($actualDistance+$training->distance>$finalDistance){
                    echo '<div class="progress-bar" role="progressbar" style="width: '.((($finalDistance-$actualDistance)/$finalDistance)*100).'%;"></div>';
                    $actualDistance = $finalDistance;
                    break;
                }
                else{
                    $actualDistance += $training->distance;
                }
                echo '<div class="progress-bar" role="progressbar" style="width: '.(($training->distance/$finalDistance)*100).'%;"></div>';
            }
        }
        else{
            foreach ($trainings as $training){
                if ($actualDistance+$training->distance>$finalDistance){
                    echo '<div class="progress-bar '.color($training->iduser).'" role="progressbar" style="width: '.((($finalDistance-$actualDistance)/$finalDistance)*100).'%;"></div>';
                    $actualDistance = $finalDistance;
                    break;
                }
                else{
                    $actualDistance += $training->distance;
                }
                echo '<div class="progress-bar '.color($training->iduser).'" role="progressbar" style="width: '.(($training->distance/$finalDistance)*100).'%;"></div>';
            }
        }

        ?>
    </div>
    <h4>
        <?php
        echo 'Prejdené '.$actualDistance.' km z '.$finalDistance.' km';

        if($mode==2){
            echo '<ul>';
            for($i = 0; $i<6; $i++){
                foreach ($trainings as $training){
                    if ($training->iduser==$i){
                        echo '<li class="'.colorLegend($training->iduser).'">'.$training->name.'</li>';
                        break;
                    }
                }
            }
            echo '</ul>';
        }
        ?>
    </h4>
    <?php if($_GET['active']==1) echo '<a href="adddistancetoroute.php?rid='.$_GET['route'].'&mode='.$_GET['mode'].'"><button class="btn btn-success">Pridať tréning</button></a> ';?>

</div>


<script src="assets/js/bootstrap/jquery-3-2-1-slim.js"></script>
<script src="assets/js/bootstrap/popper-1-12-3.js"></script>
<script src="assets/js/bootstrap/bootstrap_v4-0-0-beta-2.js"></script>
</body>
</html>