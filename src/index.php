<?php
/**
/**
 * Created by IntelliJ IDEA.
 * User: Denisek
 * Date: 5/11/2018
 * Time: 09:50 PM
 */
//homepage
?>


<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>RunTracker</title>
    <link rel="icon" type="image/jpg" href="assets/img/favicon.png">
    <link rel="stylesheet" href="assets/css/bootstrap/bootstrap_v4-0-0-beta-2.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
<?php

$role=0;
$map = 1;
include_once "menu.php";

if(empty($_SESSION)) {
    session_start();
}
if(isset($_SESSION['email']))
{
    header("location:userindex.php");

}
?>


<div class="container" id="wrapper">
    <h2 class="text-center mt-2">Vitajte v aplikácii RunTracker</h2>
    <h5 class="text-center mt-5">Mapa návštev</h5>
    <div id="map" style="height:500px;" ></div>
</div>
<div id="progress" style="display: none">
    <p class="text-center py-3">Načítavam userov <img class="mx-auto d-block mb-3" style="width: 1.8%" src="assets/img/earth_loader.gif"></p>

</div>



<script src="assets/js/bootstrap/jquery-3-2-1-slim.js"></script>
<script src="assets/js/bootstrap/popper-1-12-3.js"></script>
<script src="assets/js/bootstrap/bootstrap_v4-0-0-beta-2.js"></script>
<script src="assets/js/home-core.js"></script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAsMZqqT3Azn1HQF5tk8pG6BYW9Qp4s6LY&callback=initMap">
</script>
</body>
