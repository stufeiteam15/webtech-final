<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Prihlásenie</title>
    <link rel="icon" type="image/jpg" href="assets/img/favicon.png">
    <link rel="stylesheet" href="assets/css/bootstrap/bootstrap_v4-0-0-beta-2.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <style>
        a i{
            color: #515151;
        }
        a i:hover{
            color: #000000;
        }

        #tittle{
            padding-bottom: 2px;
            padding-top: 10px;
            font-size: 2px;
        }

        .vertical-center {
            height: 100%;
            width: 100%;
            text-align: center;
            background-color: transparent;
        }
        a:link {
            color: white;
            text-decoration: none;
        }


        a:visited {
            color: white;
            text-decoration: none;
        }


        a:hover {
            color: silver;
            text-decoration: none;
        }


        a:active {
            color: white;
            text-decoration: none;
        }
    </style>
</head>
<body>
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.debug.js"></script>

<?php
if(empty($_SESSION)) {
    session_start();
}
if (isset($_SESSION["admin"]))
    $role=1;
else
    $role=2;

include_once 'menu.php';
?>
<div class="container" id="test wrapper">
    <div class="jumbotron vertical-center" id="tittle">
        <h1 class="display-5">Štatistika tréningov</h1>
    </div>
    <?php

    /**
     * Created by IntelliJ IDEA.
     * User: Pongy
     * Date: 17. 5. 2018
     * Time: 16:21
     */
    if(empty($_SESSION)) {
        session_start();
    }

    if(isset($_SESSION['email']))
    {

    }else{
        header("location:index.php");
    }

    require_once 'assets/config/config.php';
    require_once 'functions.php';

    if (!isset($_GET['sortby'])){
        header("Location:userstats.php?uid=".$_GET['uid']."&sortby=timeBegin");
        exit;}



    $result = json_decode(callAPI("GET", $api . $_GET['uid'] . "/user-stats/" . $_GET['sortby']));
    echo "<table class=\"table table-hover\">";
    echo "<thead class=\"thead-dark\">";
    echo "<tr>";
    echo "<th scope=\"col\"><a href='../src/userstats.php?uid=".$_GET['uid']."&sortby=kilometers'>Km</th>";
    echo "<th scope=\"col\"><a href='../src/userstats.php?uid=".$_GET['uid']."&sortby=timeBegin'>Začiatok</th>";
    echo "<th scope=\"col\"><a href='../src/userstats.php?uid=".$_GET['uid']."&sortby=timeEnd'>Koniec</th>";
    echo "<th scope=\"col\"><a href='../src/userstats.php?uid=".$_GET['uid']."&sortby=cordsBeginLat'>Od(Lat)</th>";
    echo "<th scope=\"col\"><a href='../src/userstats.php?uid=".$_GET['uid']."&sortby=cordsBeginLon'>Od(Lon)</th>";
    echo "<th scope=\"col\"><a href='../src/userstats.php?uid=".$_GET['uid']."&sortby=cordsEndLat'>Do(Lat)</th>";
    echo "<th scope=\"col\"><a href='../src/userstats.php?uid=".$_GET['uid']."&sortby=cordsEndLon'>Do(Lon)</th>";
    echo "<th scope=\"col\"><a href='../src/userstats.php?uid=".$_GET['uid']."&sortby=rating'>Hodnotenie</th>";
    echo "<th scope=\"col\"><a href='../src/userstats.php?uid=".$_GET['uid']."&sortby=note'>Poznámka</th>";
    echo "<th scope=\"col\"><a href='../src/userstats.php?uid=".$_GET['uid']."&sortby=note'>Priemerná rýchlosť</th>";
    echo"</tr>";
    echo"</thead>";
    echo"<tbody>";
    $km = 0;
    foreach ($result as $singleday) {
        $date_a = new DateTime($singleday->timeBegin);
        $date_b = new DateTime($singleday->timeEnd);

        $str = 'Tue Dec 15 2009';
        $timestamp = round((strtotime($singleday->timeEnd) - strtotime($singleday->timeBegin))/3600);



        $km = $km + $singleday->kilometers;
        $time1 = date_parse($singleday->timeBegin);
        $time2 = date_parse($singleday->timeEnd);
        echo "<tr>";
        echo "<td>" . $singleday->kilometers . "</td>";
        if ($time1["minute"]<10)
            echo "<td>" . $time1["hour"]  . ":0" .  $time1["minute"] ." ". $time1["day"] . "." .  $time1["month"] . "." .  $time1["year"] .  "</td>";
        else
            echo "<td>" . $time1["hour"]  . ":" .  $time1["minute"] ." ". $time1["day"] . "." .  $time1["month"] . "." .  $time1["year"] .  "</td>";

        if ($time2["minute"]<10)
            echo "<td>" . $time2["hour"]  . ":0" .  $time2["minute"] ." ". $time2["day"] . "." .  $time2["month"] . "." .  $time2["year"] .  "</td>";
        else
            echo "<td>" . $time2["hour"]  . ":" .  $time2["minute"] ." ". $time2["day"] . "." .  $time2["month"] . "." .  $time2["year"] .  "</td>";

        echo "<td>" . $singleday->cordsBeginLat . "</td>";
        echo "<td>" . $singleday->cordsBeginLon . "</td>";
        echo "<td>" . $singleday->cordsEndLat . "</td>";
        echo "<td>" . $singleday->cordsEndLon . "</td>";
        echo "<td>" . $singleday->rating . "</a></td>";
        echo "<td>" . $singleday->note . "</td>";
        echo "<td>" . round($singleday->kilometers/$timestamp, 1) . " km/h </td>";
        echo "</tr>";
    }

    ?>

    </tbody>
    </table>
    <div class="jumbotron vertical-center">
        <div class="container">
            <h1 class="display-4"><?php echo round($km/sizeof($result))?> km</h1>
            <p class="lead">Priemerná vzdialenosť</p>
            <i class="far fa-file-pdf fa-2x" id="cmd"></i>
        </div>
    </div>
</div>
<script>
    (function(){
        var
            form = $('.container'),
            cache_width = form.clientWidth,

            a4  =[ 725,  842];

        $('#cmd').on('click',function(){
            $('body').scrollTop(0);
            createPDF();
        });

        function createPDF(){
            getCanvas().then(function(canvas){
                var
                    img = canvas.toDataURL("image/png"),
                    doc = new jsPDF({
                        unit:'px',
                        format:'a3'
                    });
                doc.addImage(img, 'JPEG', 1, 25);
                doc.save('statistiky.pdf');
                form.width(cache_width);
                location.reload();
            });

        }

// create canvas object
        function getCanvas(){
            form.width((a4[0]*1.5)).css('max-width','none');
            return html2canvas(form,{
                imageTimeout:2000,
                removeContainer:true
            });
        }

    }());
</script>
<script type="text/javascript" src="//cdn.rawgit.com/niklasvh/html2canvas/0.5.0-alpha2/dist/html2canvas.min.js"></script>
<script src="assets/js/bootstrap/jquery-3-2-1-slim.js"></script>
<script src="assets/js/bootstrap/popper-1-12-3.js"></script>
<script src="assets/js/bootstrap/bootstrap_v4-0-0-beta-2.js"></script>

</body>
</html>