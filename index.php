<?php
/**
 * Created by IntelliJ IDEA.
 * User: tomas
 * Date: 05/04/2018
 * Time: 13:45
 */

?>

<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Final zadanie</title>
    <link rel="icon" type="image/jpg" href="src/assets/img/favicon.png">
    <link rel="stylesheet" href="src/assets/css/bootstrap/bootstrap_v4-0-0-beta-2.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
    <link rel="stylesheet" href="src/assets/css/style.css">
</head>
<body>

<div class="container">
    <h2 class="text-info text-center py-5">
        <i class="fab fa-angellist pr-1"></i>Hello, world!
    </h2>

    <div class="row justify-content-center">
        <div class="col-auto">
            <h5>Tím 15:</h5>
            <ul class="list-group list-group-flush">

                <!-- TODO: change "fas fa-ban text-danger" to "far fa-check-circle text-success" -->

                <li class="list-group-item"><i class="far fa-check-circle text-success pr-2"></i>Tomáš Šromovský</li>
                <li class="list-group-item"><i class="far fa-check-circle text-success pr-2"></i>Denis Šmátrala</li>
                <li class="list-group-item"><i class="far fa-check-circle text-success pr-2"></i>Stanislav Bekeš</li>
                <li class="list-group-item"><i class="far fa-check-circle text-success pr-2"></i>Samuel Šušoliak</li>
                <li class="list-group-item"><i class="far fa-check-circle text-success pr-2"></i>Filip Ujčik</li>
            </ul>
        </div>
    </div>
</div>

<!-- Bootstrap core -->
<script src="src/assets/js/bootstrap/jquery-3-2-1-slim.js"></script>
<script src="src/assets/js/bootstrap/popper-1-12-3.js"></script>
<script src="src/assets/js/bootstrap/bootstrap_v4-0-0-beta-2.js"></script>

</body>