<?php
/**
 * Created by IntelliJ IDEA.
 * User: tomas
 * Date: 16/05/2018
 * Time: 16:35
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RepoController extends Controller {

    public function __construct()
    {
        //
    }

    public function getAddresses() {
        $response = DB::select("SELECT BYDLISKO_ULICA AS street, BYDLISKO_OBEC AS city, BYDLISKO_PSC AS psc FROM USERS");
        return response()->json($response);
    }

    public function getSchools() {
        $response = DB::select("SELECT SKOLA AS name, SKOLA_ADRESA AS address FROM USERS");
        return response()->json($response);
    }

    public function addRoute(Request $request, $id) {
        $response = new \stdClass();
        $route = new \stdClass();

        $route->name = $request->input('name');
        $route->distance = $request->input('distance');
        $route->mode = $request->input('mode');
        $route->startLocLat = $request->input('startLocLat');
        $route->startLocLng = $request->input('startLocLng');
        $route->endLocLat = $request->input('endLocLat');
        $route->endLocLng = $request->input('endLocLng');
        $route->startState = $request->input('startState');
        $route->endState = $request->input('endState');

        $route->distance = $route->distance ? $route->distance : "NULL";
        $route->startLocLat = $route->startLocLat ? $route->startLocLat : "NULL";
        $route->startLocLng = $route->startLocLng ? $route->startLocLng : "NULL";
        $route->endLocLat = $route->endLocLat ? $route->endLocLat : "NULL";
        $route->endLocLng = $route->endLocLng ? $route->endLocLng : "NULL";

        $sql = "INSERT INTO CIEL (MENO, KRAJINA_A, KRAJINA_B, START_LAT, START_LON, CIEL_LAT, CIEL_LON, MODE, OWNER_ID, VZDIALENOST) VALUES ('$route->name', '$route->startState', '$route->endState', $route->startLocLat, $route->startLocLng, $route->endLocLat, $route->endLocLng, $route->mode, $id, $route->distance)";
        $response->value = DB::insert($sql);

        $idRoute = DB::getPdo()->lastInsertId();

        if ($route->mode == 1) {
            $sql = "INSERT INTO CIEL_USER (ID_CIEL, ID_USER, MODE, ACTIVE) VALUES ($idRoute, $id, 1, 0)";
            $response->value = DB::insert($sql);
        } elseif ($route->mode == 2) {
            $runners = $request->input('runners');
            $response->value = true;
            for ($i = 0; $i < sizeof($runners); $i++) {
                $userId = $runners[$i]['id'];
                $sql = "INSERT INTO CIEL_USER (ID_CIEL, ID_USER, MODE, ACTIVE) VALUES ($idRoute, $userId, 2, 0)";
                $ok = DB::insert($sql);
                if (!$ok) {
                    $response->value = false;
                }
            }
        }

        return response()->json($response);
    }

    public function activateRoute(Request $request, $id) {
        $idRoute = $request->input('idroute');
        $route = DB::select("SELECT ID AS id, MODE AS mode FROM CIEL WHERE ID = $idRoute")[0];
        $mode = $route->mode;

        $sql = "UPDATE CIEL_USER SET ACTIVE = 0 WHERE ID_USER = $id AND MODE = $mode";
        DB::update($sql);

        $exist = DB::select("SELECT * FROM CIEL_USER WHERE ID_USER = $id AND ID_CIEL = $idRoute AND MODE = $mode");

        if ($exist) {
            $sql = "UPDATE CIEL_USER SET ACTIVE = 1 WHERE ID_USER = $id AND ID_CIEL = $idRoute AND MODE = $mode";
            DB::update($sql);
        } else {
            $sql = "INSERT INTO CIEL_USER (ID_CIEL, ID_USER, MODE, ACTIVE) VALUES ($idRoute, $id, $mode, 1)";
            DB::insert($sql);
        }
    }

    public function deactivateRoute(Request $request, $id) {
        $idRoute = $request->input('id_trasa');

        $sql = "UPDATE CIEL_USER SET ACTIVE = 0 WHERE ID_USER = $id AND ID_CIEL = $idRoute";
        DB::update($sql);
    }

    public function getRoutes($id, $active = null) {
        $data = array();
        if ($active == 'active') {
            $routes = DB::select("SELECT ID_CIEL AS routeId FROM CIEL_USER WHERE ID_USER = $id AND ACTIVE = 1");
            for ($i = 0; $i < sizeof($routes); $i++) {
                $routeId = $routes[$i]->routeId;
                $route = DB::select("SELECT ID AS id, MENO AS name, MODE as mode FROM CIEL WHERE ID = $routeId");
                array_push($data, $route[0]);
            }
        } elseif ($active == 'inactive') {
            $routes = DB::select("SELECT ID_CIEL AS routeId, ACTIVE as active FROM CIEL_USER WHERE ID_USER = $id AND MODE != 0  AND ACTIVE = 0");

            $activePublic = DB::select("SELECT ID_CIEL AS routeId FROM CIEL_USER WHERE ID_USER = $id AND MODE = 0  AND ACTIVE = 1");

            $ap = array();
            for ($i = 0; $i < sizeof($activePublic); $i++) {
                array_push($ap, $activePublic[$i]->routeId);
            }

            for ($i = 0; $i < sizeof($routes); $i++) {
                $routeId = $routes[$i]->routeId;
                $route = DB::select("SELECT ID AS id, MENO AS name, MODE as mode FROM CIEL WHERE ID = $routeId");
                array_push($data, $route[0]);
            }
            $routes = DB::select("SELECT ID AS id, MENO AS name, MODE as mode FROM CIEL WHERE MODE = 0");
            for ($i = 0; $i < sizeof($routes); $i++) {

                if (!in_array($routes[$i]->id, $ap)) {
                    array_push($data, $routes[$i]);
                }
            }
        }

        return response()->json($data);
    }

    public function importUsers(Request $request) {
        $response = new \stdClass();

        $counterOK = 0;
        $counterERR = 0;
        $data = $request->input('data');

        for ($i = 0; $i < sizeof($data); $i++) {
            $user = new \stdClass();

            $user->meno = $data[$i]['name'];
            $user->priezvisko = $data[$i]['surname'];
            $user->email = $data[$i]['email'];
            $user->skola = $data[$i]['schoolName'];
            $user->sk_adresa = $data[$i]['schoolAddress'];
            $user->b_ulica = $data[$i]['street'];
            $user->b_obec = $data[$i]['city'];
            $user->b_psc = $data[$i]['psc'];

            $news = 0;
            $password = "123456";

            if ($user->email) {

                $exist = DB::select("SELECT * FROM USERS WHERE EMAIL LIKE '$user->email'");

                if (!$exist) {
                    $hash_pass = password_hash($password, PASSWORD_DEFAULT);
                    $ok = DB::insert("INSERT INTO USERS (MENO, PRIEZVISKO, EMAIL, SKOLA, SKOLA_ADRESA, BYDLISKO_ULICA, BYDLISKO_OBEC, BYDLISKO_PSC, HESLO, USER_ROLE, NEWSLETTER) VALUES ('$user->meno', '$user->priezvisko', '$user->email', '$user->skola', '$user->sk_adresa', '$user->b_ulica', '$user->b_obec', '$user->b_psc', '$hash_pass', 1, $news)");
                    if ($ok) {
                        $counterOK++;
                    } else {
                        $counterERR++;
                    }
                }
            } else {
                $counterERR++;
            }
        }

        $response->all = sizeof($data);
        $response->successful = $counterOK;
        $response->failed = $counterERR;
        return response()->json($response);
    }

    public function addTraining(Request $request, $uid, $rid) {
        $sql = "SELECT ID AS id FROM CIEL_USER WHERE ID_USER = $uid AND ID_CIEL = $rid";
        $cu = DB::select($sql);

        if (!$cu) {
            return response()->json(null);
        }

        $idCU = $cu[0]->id;

        $training = new \stdClass();
        $training->distance = $request->input('distance');
        $training->startTime = $request->input('startTime');
        $training->endTime = $request->input('endTime');
        $training->rating = $request->input('rating');
        $training->startLat = $request->input('startLat');
        $training->startLon = $request->input('startLon');
        $training->endLat = $request->input('endLat');
        $training->endLon = $request->input('endLon');
        $training->note = $request->input('note');

        $training->distance = $training->distance ? $training->distance : "NULL";
        $training->rating = $training->rating ? $training->rating : "NULL";
        $training->startLat = $training->startLat ? $training->startLat : "NULL";
        $training->startLon = $training->startLon ? $training->startLon : "NULL";
        $training->endLat = $training->endLat ? $training->endLat : "NULL";
        $training->endLon = $training->endLon ? $training->endLon : "NULL";

        $sql = "INSERT INTO TRENING (DLZKA_KM, CAS_START, CAS_KONIEC, START_LAT, START_LON, CIEL_LAT, CIEL_LON, HODNOTENIE, POZNAMKA, ID_CIEL_USER) VALUES ($training->distance, '$training->startTime', '$training->endTime', $training->startLat, $training->startLon, $training->endLat, $training->endLon, $training->rating, '$training->note', $idCU)";
        DB::insert($sql);
    }

    public function getTrainings($id, $sort) {
        $sql = "SELECT ID AS id FROM CIEL_USER WHERE ID_USER = $id";
        $data = DB::select($sql);

        $sql = "";
        for ($i = 0; $i < sizeof($data); $i++) {
            $idCU = $data[$i]->id;

            $sql .= "SELECT DLZKA_KM AS kilometers, CAS_START AS timeBegin, CAS_KONIEC AS timeEnd, START_LON AS cordsBeginLon, START_LAT AS cordsBeginLat, CIEL_LON AS cordsEndLon, CIEL_LAT AS cordsEndLat, HODNOTENIE AS rating, POZNAMKA AS note FROM TRENING WHERE ID_CIEL_USER = $idCU";
            if (sizeof($data) - 1 != $i) {
                $sql .= " UNION ";
            }
            if (sizeof($data) - 1 == $i) {
                $sql .= " ORDER BY $sort";
            }
        }

        $data = DB::select($sql);

        return response()->json($data);
    }

    public function getNews() {
        $sql = "SELECT BODY AS new, SUBJECT AS header, DATE AS date FROM NEWSLETTER ORDER BY DATE DESC LIMIT 3";
        $data = DB::select($sql);

        return response()->json($data);
    }

    public function getUsers() {
        $sql = "SELECT ID AS id, MENO AS name, PRIEZVISKO AS surname, EMAIL AS email, SKOLA AS schoolName, SKOLA_ADRESA AS schoolAddress, BYDLISKO_ULICA AS street, BYDLISKO_PSC AS psc, BYDLISKO_OBEC AS city FROM USERS";
        $data = DB::select($sql);

        return response()->json($data);
    }

    public function addNews(Request $request) {
        $data = new \stdClass();

        $data->body = $request->input('new');
        $data->header = $request->input('newheader');

        DB::insert("INSERT INTO NEWSLETTER (SUBJECT, BODY) VALUES ('$data->header', '$data->body')");

        $users = DB::select("SELECT EMAIL AS email FROM USERS WHERE NEWSLETTER = 1");
        for ($i = 0; $i < sizeof($users); $i++) {
            $email = $users[$i]->email;

            $message = "
            <html>
            <head>
                <title>RunTracker</title>
            </head>
            <body>
                <h1>Newsletter</h1>
                <h3>Novinky zo sveta atlétov</h3><br>
                <span style='font-weight: bold'>{$data->header}</span><br><br>
                {$data->body}<br><br>
                <a href='http://webtech.sromovsky.sk/final/src/'>www.runtracker.com</a>
            </body>
            </html>
            ";
            $subject = "Newsletter";

            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type: text/html; charset=utf-8" . "\r\n";
            $headers .= "From: newsletter@runtracker.sk" . "\r\n";
            $headers .= "Reply-To: " . $email . "\r\n";
            $headers .= "CC: ". $email ."\r\n";
            $headers .= "X-Mailer: PHP/" . phpversion() . "\r\n";
            $headers .= "X-Priority: 2\nX-MSmail-Priority: high";

            mail($email, $subject, $message, $headers);
        }
    }

    public function getRouteById($rid) {
        $data = DB::select("SELECT MENO AS name, VZDIALENOST AS distance, KRAJINA_A AS startState, KRAJINA_B AS endState, START_LAT AS startLat, START_LON AS startLon, CIEL_LAT AS endLat, CIEL_LON AS endLon, MODE AS mode FROM CIEL WHERE ID = $rid");
        $data = $data ? $data[0] : null;
        return response()->json($data);
    }

    public function getTrainingsByUserRoute($idUser, $idRoute) {
        $sql = "SELECT ID AS id FROM CIEL_USER WHERE ID_USER = $idUser AND ID_CIEL = $idRoute";
        $cu = DB::select($sql);

        if (!$cu) {
            return response()->json(array());
        }

        $idCU = $cu[0]->id;

        $sql = "SELECT ID AS id, CAS_START AS trainingBegin, CAS_KONIEC AS trainingEnd, DLZKA_KM AS distance, ADD_TIME AS addTime FROM TRENING WHERE ID_CIEL_USER = $idCU";
        $data = DB::select($sql);

        return response()->json($data);
    }

    public function getRelayProgress($idRoute) {
        $sql = "SELECT ID AS cuId, ID_USER AS userId FROM CIEL_USER WHERE ID_CIEL = $idRoute";
        $users = DB::select($sql);

        $data = array();

        for ($i = 0; $i < sizeof($users); $i++) {
            $cuId = $users[$i]->cuId;
            $userId = $users[$i]->userId;
            $user = DB::select("SELECT MENO AS name, PRIEZVISKO AS surname FROM USERS WHERE ID = $userId")[0];

            $users[$i]->iduser = $i;
            $users[$i]->name = $user->name;
            $users[$i]->surname = $user->surname;

            $trainings = DB::select("SELECT ID AS trainingId, DLZKA_KM AS distance, ADD_TIME AS date FROM TRENING WHERE ID_CIEL_USER = $cuId");
            for ($j = 0; $j < sizeof($trainings); $j++) {
                $training = $trainings[$j];
                $training->iduser = $i;
                $training->name = $users[$i]->name . ' ' . $users[$i]->surname;

                array_push($data, $training);
            }
        }
        return response()->json($data);
    }
}