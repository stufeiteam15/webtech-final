<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function setPassword(Request $request, $id) {
        $hash_pass = password_hash($request->input('password'), PASSWORD_DEFAULT);
        DB::update("UPDATE USERS SET HESLO = '$hash_pass', VERIFIED = 1 WHERE ID = $id");
    }

    public function login(Request $request) {
        $response = new \stdClass();

        $email = $request->input('email');
        $password = $request->input('password');

        $user = DB::select("SELECT ID AS id, HESLO AS pass, USER_ROLE AS role, VERIFIED As verify FROM USERS WHERE EMAIL LIKE '$email'");

        if (!$user) {
            $response->value = false;
            return response()->json($response);
        }

        if (!password_verify($password, $user[0]->pass)) {
            $response->value = false;
            return response()->json($response);
        }

        $response->value = true;
        $response->role = $user[0]->role == 2 ? 2 : $user[0]->verify;
        $response->userId = $user[0]->id;


        return response()->json($response);
    }

    public function register(Request $request) {
        $user = new \stdClass();
        $response = new \stdClass();

        $user->meno = $request->input('name');
        $user->priezvisko = $request->input('surname');
        $user->email = $request->input('email');
        $user->skola = $request->input('schoolName');
        $user->sk_adresa = $request->input('schoolAddress');
        $user->b_ulica = $request->input('street');
        $user->b_obec = $request->input('city');
        $user->b_psc = $request->input('psc');

        $news = $request->input('newsletter') ? 1 : 0;

        if (!$user->email) {
            $response->value = false;
            return response()->json($response);
        }

        $exist = DB::select("SELECT * FROM USERS WHERE EMAIL LIKE '$user->email'");

        if ($exist) {
            $response->value = false;
            return response()->json($response);
        }

        $password = $this->randomPassword();
        $hash_pass = password_hash($password, PASSWORD_DEFAULT);

        DB::insert("INSERT INTO USERS (MENO, PRIEZVISKO, EMAIL, SKOLA, SKOLA_ADRESA, BYDLISKO_ULICA, BYDLISKO_OBEC, BYDLISKO_PSC, HESLO, USER_ROLE, NEWSLETTER) VALUES ('$user->meno', '$user->priezvisko', '$user->email', '$user->skola', '$user->sk_adresa', '$user->b_ulica', '$user->b_obec', '$user->b_psc', '$hash_pass', 1, $news)");

        $to = $user->email;
        $message = "
            <html>
            <head>
                <title>RunTracker</title>
            </head>
            <body>
                <h1>Vitajte v aplikácii RunTracker</h1>
                <h3>Vaše nové prihlasovacie údaje sú:</h3>
                <span style='font-weight: bold'>Login: </span>{$to}<br>
                <span style='font-weight: bold'>Heslo: </span>{$password}<br><br><br>
                <a href='http://webtech.sromovsky.sk/final/src/'>www.runtracker.com</a>
            </body>
            </html>
";
        $subject = "New registration";

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type: text/html; charset=utf-8" . "\r\n";
        $headers .= "From: registration@runtracker.sk" . "\r\n";
        $headers .= "Reply-To: " . $to . "\r\n";
        $headers .= "CC: ". $to ."\r\n";
        $headers .= "X-Mailer: PHP/" . phpversion() . "\r\n";
        $headers .= "X-Priority: 2\nX-MSmail-Priority: high";

        mail($to, $subject, $message, $headers);

        $response->value = true;
        return response()->json($response);
    }

    function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }
}
