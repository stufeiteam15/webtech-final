<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// Auth - controllers:
$router->post('/has-valid-login', 'AuthController@login');
$router->post('/has-valid-reg', 'AuthController@register');
$router->post('/{id}/setpassword', 'AuthController@setPassword');

// Repo - controllers:
$router->get('/addresses', 'RepoController@getAddresses');
$router->get('/schools', 'RepoController@getSchools');
$router->get('/news', 'RepoController@getNews');
$router->get('/users', 'RepoController@getUsers');
$router->get('/routes/{rid}', 'RepoController@getRouteById');
$router->get('/{id}/routes[-{active}]', 'RepoController@getRoutes');
$router->get('/{id}/user-stats[/{sort}]', 'RepoController@getTrainings');
$router->get('/{idRoute}/progressteam', 'RepoController@getRelayProgress');
$router->get('/{idUser}/{idRoute}/progessone', 'RepoController@getTrainingsByUserRoute');

$router->post('/news', 'RepoController@addNews');
$router->post('/import-users', 'RepoController@importUsers');
$router->post('/{id}/add-route', 'RepoController@addRoute');
$router->post('/{id}/activate-route', 'RepoController@activateRoute');
$router->post('/{id}/deactivate-route', 'RepoController@deactivateRoute');
$router->post('/{uid}/{rid}/add-training', 'RepoController@addTraining');
